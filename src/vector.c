#define _POSIX_C_SOURCE 200809L
#include <collections/vector.h>

#include <errno.h>
#include <string.h>

static int resize(struct _vector *vector, size_t capacity, size_t size)
{
	size_t alloc_size = capacity * size;

	if (size != 0 && alloc_size / size != capacity)
		// Integer overflow occurred
		return -EOVERFLOW;

	void *new_data = alloc_realloc(&vector->allocator, vector->data, alloc_size);
	if (new_data == NULL && alloc_size != 0)
		return -ENOMEM;

	vector->data = new_data;
	vector->capacity = capacity;
	if (capacity < vector->len)
		vector->len = capacity;

	return 0;
}

int _vector_reserve(struct _vector *vector, size_t additional, size_t size)
{
	size_t capacity = vector->len + additional;
	if (capacity <= vector->capacity) {
		return 0;
	}
	if (capacity < 8) {
		capacity = 8;
	}
	if (capacity < vector->capacity * 2) {
		capacity = vector->capacity * 2;
	}
	return resize(vector, capacity, size);
}

int _vector_insert(
	struct _vector *restrict vector,
	size_t index,
	const void *restrict item,
	size_t n,
	size_t size)
{
	if (index > vector->len)
		return -EINVAL;

	int ret = _vector_reserve(vector, n, size);
	if (ret)
		return ret;

	void *target = _vector_get(vector, index, size);

	if (index != vector->len)
		memmove(_vector_get(vector, index + n, size), target, (vector->len - index) * size);

	memcpy(target, item, size * n);
	vector->len += n;
	return 0;
}

int _vector_remove(
	struct _vector *restrict vector, size_t index, void *restrict item, size_t n, size_t size)
{
	if (n == 0 || index + n - 1 >= vector->len)
		return -EINVAL;

	void *target = _vector_get(vector, index, size);

	if (item)
		memcpy(item, target, size * n);

	vector->len -= n;
	if (index != vector->len)
		memmove(target, _vector_get(vector, index + n, size), (vector->len - index) * size);

	size_t capacity = vector->capacity;
	while (vector->len < capacity / 2) {
		capacity /= 2;
	}
	capacity = capacity >= 8 ? capacity : 8;

	if (capacity < vector->capacity)
		return resize(vector, capacity, size);
	return 0;
}

static void memswap(void *a, void *b, size_t size)
{
	size_t words = size / sizeof(int);
	for (size_t i = 0; i < words; ++i) {
		int *x = ((int *) a) + i;
		int *y = ((int *) b) + i;

		int tmp = *x;
		*x = *y;
		*y = tmp;
	}

	size_t bytes = size % sizeof(int);
	for (size_t i = 0; i < bytes; ++i) {
		char *x = ((char *) a) + sizeof(int) * words + i;
		char *y = ((char *) b) + sizeof(int) * words + i;

		char tmp = *x;
		*x = *y;
		*y = tmp;
	}
}

void _vector_sort(
	const struct _vector *restrict vector,
	size_t size,
	int (*f)(const void *, const void *, void *),
	void *restrict ctx)
{
	int swapped;
	do {
		swapped = 0;
		for (size_t i = 1; i < vector->len; ++i) {
			void *a = _vector_get(vector, i - 1, size);
			void *b = _vector_get(vector, i, size);
			if (f(a, b, ctx) > 0) {
				memswap(a, b, size);
				swapped = 1;
			}
		}
	} while (swapped);
}

void *_vector_binary_search(
	const struct _vector *restrict vector,
	size_t size,
	int (*f)(const void *, void *),
	void *restrict ctx,
	size_t *restrict index)
{
	ssize_t head = 0;
	ssize_t tail = ((ssize_t) vector->len) - 1;
	while (head <= tail) {
		size_t i = ((size_t) (head + tail)) / 2;
		void *val = _vector_get(vector, i, size);

		int ret = f(val, ctx);
		if (ret < 0) {
			head = i + 1;
		} else if (ret > 0) {
			tail = i - 1;
		} else {
			*index = i;
			return val;
		}
	}
	*index = head;
	return NULL;
}
