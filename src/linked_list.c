#define _POSIX_C_SOURCE 200809L
#include <collections/linked_list.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>

struct list_node {
	void *next;
	void *prev;
	char data[];
};

int _linked_list_push(struct _linked_list *restrict list, const void *restrict data, size_t size)
{
	struct list_node *node = alloc_alloc(&list->allocator, sizeof(*node) + size);
	if (node == NULL)
		return -ENOMEM;

	memcpy(&node->data, data, size);

	node->next = list->head;
	node->prev = NULL;

	list->head = node;
	if (list->tail == NULL)
		list->tail = node;

	return 0;
}

int _linked_list_pop(struct _linked_list *restrict list, void *restrict item, size_t size)
{
	struct list_node *node = list->head;
	list->head = node->next;

	if (list->tail == node)
		list->tail = NULL;

	if (item)
		memcpy(item, node->data, size);

	alloc_dealloc(&list->allocator, node);
	return 0;
}
