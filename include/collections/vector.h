#pragma once
#include <collections/_common.h>

#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>

#include <alloc.h>

#if defined(__cplusplus)
extern "C" {
#endif

struct _vector {
	struct alloc allocator;
	void *data;
	size_t len;
	size_t capacity;
};

COLLECTIONS_PUBLIC int _vector_reserve(struct _vector *vector, size_t additional, size_t size);

static inline int _vector_init(
	struct _vector *restrict vector,
	size_t capacity,
	size_t size,
	const struct alloc *restrict allocator)
{
	vector->allocator.vtable = allocator->vtable;
	vector->allocator.context = allocator->context;
	vector->data = NULL;
	vector->len = 0;
	vector->capacity = 0;

	if (capacity == 0)
		return 0;
	return _vector_reserve(vector, capacity, size);
}

static inline void _vector_free(struct _vector *vector)
{
	alloc_dealloc(&vector->allocator, vector->data);
	vector->data = NULL;
	vector->len = 0;
	vector->capacity = 0;
}

static inline void *_vector_get(const struct _vector *vector, size_t index, size_t size)
{
	return &((unsigned char *) vector->data)[index * size];
}

COLLECTIONS_PUBLIC int _vector_insert(
	struct _vector *restrict vector,
	size_t index,
	const void *restrict item,
	size_t n,
	size_t size);

COLLECTIONS_PUBLIC int _vector_remove(
	struct _vector *restrict vector, size_t index, void *restrict item, size_t n, size_t size);

static inline int _vector_push(
	struct _vector *restrict vector, const void *restrict item, size_t size)
{
	return _vector_insert(vector, vector->len, item, 1, size);
}

static inline int _vector_pop(struct _vector *restrict vector, void *restrict item, size_t size)
{
	return _vector_remove(vector, vector->len - 1, item, 1, size);
}

COLLECTIONS_PUBLIC void _vector_sort(
	const struct _vector *restrict vector,
	size_t size,
	int (*f)(const void *, const void *, void *),
	void *restrict ctx);

COLLECTIONS_PUBLIC void *_vector_binary_search(
	const struct _vector *restrict vector,
	size_t size,
	int (*f)(const void *, void *),
	void *restrict ctx,
	size_t *restrict index);

#define VECTOR_DEFINE(NAME, TYPE)                                                                  \
	struct NAME;                                                                               \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_init(                                                \
		struct NAME *restrict vector,                                                      \
		size_t capacity,                                                                   \
		const struct alloc *restrict allocator)                                            \
	{                                                                                          \
		return _vector_init((struct _vector *) vector, capacity, sizeof(TYPE), allocator); \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_reserve(struct NAME *vector, size_t additional)      \
	{                                                                                          \
		return _vector_reserve((struct _vector *) vector, additional, sizeof(TYPE));       \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline void NAME##_free(struct NAME *vector)                           \
	{                                                                                          \
		_vector_free((struct _vector *) vector);                                           \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline TYPE *NAME##_get(const struct NAME *vector, size_t index)       \
	{                                                                                          \
		return _vector_get((const struct _vector *) vector, index, sizeof(TYPE));          \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_insert(                                              \
		struct NAME *restrict vector, size_t index, TYPE const *restrict item, size_t n)   \
	{                                                                                          \
		return _vector_insert((struct _vector *) vector, index, item, n, sizeof(TYPE));    \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_remove(                                              \
		struct NAME *restrict vector, size_t index, TYPE *restrict item, size_t n)         \
	{                                                                                          \
		return _vector_remove((struct _vector *) vector, index, item, n, sizeof(TYPE));    \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_push(                                                \
		struct NAME *restrict vector, TYPE const *restrict item)                           \
	{                                                                                          \
		return _vector_push((struct _vector *) vector, item, sizeof(TYPE));                \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_pop(                                                 \
		struct NAME *restrict vector, TYPE *restrict item)                                 \
	{                                                                                          \
		return _vector_pop((struct _vector *) vector, item, sizeof(TYPE));                 \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline void NAME##_sort(                                               \
		const struct NAME *restrict vector,                                                \
		int (*f)(TYPE const *, TYPE const *, void *),                                      \
		void *restrict ctx)                                                                \
	{                                                                                          \
		_vector_sort(                                                                      \
			(const struct _vector *) vector,                                           \
			sizeof(TYPE),                                                              \
			(int (*)(const void *, const void *, void *)) f,                           \
			ctx);                                                                      \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline TYPE *NAME##_binary_search(                                     \
		const struct NAME *restrict vector,                                                \
		int (*f)(TYPE const *, void *),                                                    \
		void *restrict ctx,                                                                \
		size_t *restrict index)                                                            \
	{                                                                                          \
		return _vector_binary_search(                                                      \
			(const struct _vector *) vector,                                           \
			sizeof(TYPE),                                                              \
			(int (*)(const void *, void *)) f,                                         \
			ctx,                                                                       \
			index);                                                                    \
	}                                                                                          \
                                                                                                   \
	struct NAME {                                                                              \
		struct alloc allocator;                                                            \
		TYPE *data;                                                                        \
		size_t len;                                                                        \
		size_t capacity;                                                                   \
	}

#if defined(__cplusplus)
} // extern "C"
#endif
