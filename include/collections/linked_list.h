#pragma once
#include <collections/_common.h>

#include <stddef.h>

#include <alloc.h>

#if defined(__cplusplus)
extern "C" {
#endif

struct _linked_list {
	struct alloc allocator;
	void *head;
	void *tail;
};

static inline int _linked_list_init(
	struct _linked_list *restrict list, const struct alloc *restrict allocator)
{
	list->allocator.vtable = allocator->vtable;
	list->allocator.context = allocator->context;
	list->head = NULL;
	list->tail = NULL;
	return 0;
}

static inline int _linked_list_is_empty(const struct _linked_list *list)
{
	return list->head == NULL;
}

COLLECTIONS_PUBLIC int _linked_list_push(
	struct _linked_list *restrict list, const void *restrict item, size_t size);

COLLECTIONS_PUBLIC int _linked_list_pop(
	struct _linked_list *restrict list, void *restrict item, size_t size);

#define linked_list_foreach(list, position)                                                        \
	for (position = list->head; position != NULL; position = position->next)

#define LINKED_LIST_DEFINE(NAME, TYPE)                                                             \
	struct NAME;                                                                               \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_init(                                                \
		struct NAME *restrict list, const struct alloc *restrict allocator)                \
	{                                                                                          \
		return _linked_list_init((struct _linked_list *) list, allocator);                 \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_is_empty(const struct NAME *list)                    \
	{                                                                                          \
		return _linked_list_is_empty((const struct _linked_list *) list);                  \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_push(                                                \
		struct NAME *restrict list, TYPE const *restrict elem)                             \
	{                                                                                          \
		return _linked_list_push((struct _linked_list *) list, elem, sizeof(*elem));       \
	}                                                                                          \
                                                                                                   \
	MAYBE_UNUSED static inline int NAME##_pop(struct NAME *restrict list, TYPE *restrict elem) \
	{                                                                                          \
		return _linked_list_pop((struct _linked_list *) list, elem, sizeof(*elem));        \
	}                                                                                          \
                                                                                                   \
	struct NAME##_node {                                                                       \
		struct NAME##_node *next;                                                          \
		struct NAME##_node *prev;                                                          \
		TYPE data;                                                                         \
	};                                                                                         \
                                                                                                   \
	struct NAME {                                                                              \
		struct alloc allocator;                                                            \
		struct NAME##_node *head;                                                          \
		struct NAME##_node *tail;                                                          \
	}

#if defined(__cplusplus)
} // extern "C"
#endif
