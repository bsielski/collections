#pragma once
#if defined _WIN32 || defined __CYGWIN__
#ifdef BUILDING_COLLECTIONS
#define COLLECTIONS_PUBLIC __declspec(dllexport)
#else
#define COLLECTIONS_PUBLIC __declspec(dllimport)
#endif
#else
#ifdef BUILDING_COLLECTIONS
#define COLLECTIONS_PUBLIC __attribute__((visibility("default")))
#else
#define COLLECTIONS_PUBLIC
#endif
#endif

#ifndef MAYBE_UNUSED
#define MAYBE_UNUSED __attribute__((unused))
#endif
