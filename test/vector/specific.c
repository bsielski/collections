#define _POSIX_C_SOURCE 200809L
#include <collections/vector.h>

#include <assert.h>
#include <stdio.h>

VECTOR_DEFINE(vector_int, int);

static int find_int_by_value(const int *vector_member, void *ctx)
{
	int value = *(const int *) ctx;
	return *vector_member - value;
}

static int test_int(void)
{
	struct vector_int test_vector;
	vector_int_init(&test_vector, 4, &alloc_std);
	assert(test_vector.len == 0);

	int pushed = 10;
	vector_int_push(&test_vector, &pushed);
	assert(test_vector.len == 1);

	pushed = 20;
	vector_int_push(&test_vector, &pushed);
	assert(test_vector.len == 2);

	pushed = 30;
	vector_int_push(&test_vector, &pushed);
	assert(test_vector.len == 3);

	pushed = 40;
	vector_int_push(&test_vector, &pushed);
	assert(test_vector.len == 4);

	int *gotten = vector_int_get(&test_vector, 1);
	assert(*gotten == 20);
	assert(test_vector.len == 4);

	for (size_t i = 0; i < test_vector.len; ++i) {
		gotten = vector_int_get(&test_vector, i);
		printf("test_vector[%zu] = %d\n", i, *gotten);
	}

	int popped = -1;
	vector_int_pop(&test_vector, &popped);
	assert(popped == 40);
	assert(test_vector.len == 3);

	gotten = vector_int_get(&test_vector, 2);
	assert(*gotten == 30);
	assert(test_vector.len == 3);

	popped = -1;
	vector_int_pop(&test_vector, &popped);
	assert(popped == 30);
	assert(test_vector.len == 2);

	popped = -1;
	vector_int_pop(&test_vector, &popped);
	assert(popped == 20);
	assert(test_vector.len == 1);

	popped = -1;
	vector_int_pop(&test_vector, &popped);
	assert(popped == 10);
	assert(test_vector.len == 0);

	pushed = 0;
	vector_int_push(&test_vector, &pushed);
	for (int i = 2; i < 17; ++i) {
		vector_int_push(&test_vector, &i);
	}

	const int looking_for = 1;
	size_t index;
	int *val = vector_int_binary_search(&test_vector, find_int_by_value, &looking_for, &index);
	assert(val == NULL);
	assert(index == 1);

	vector_int_free(&test_vector);
	return 0;
}

static void vector_int_compare(
	const struct vector_int *restrict vector1, const struct vector_int *restrict vector2)
{
	size_t i1 = 0;
	size_t i2 = 0;

	while (i1 < vector1->len && i2 < vector2->len) {
		int val1 = *vector_int_get(vector1, i1);
		int val2 = *vector_int_get(vector2, i2);

		printf("checking %d and %d\n", val1, val2);

		int ret = val1 - val2;
		if (ret < 0) {
			printf("%d was only found in vector1\n", val1);
			++i1;
		} else if (ret > 0) {
			printf("%d was only found in vector2\n", val2);
			++i2;
		} else {
			printf("%d was only found in both vectors\n", val1);
			++i1;
			++i2;
		}
	}

	for (; i1 < vector1->len; ++i1) {
		int val1 = *vector_int_get(vector1, i1);
		printf("%d was only found in vector1\n", val1);
	}
	for (; i2 < vector2->len; ++i2) {
		int val2 = *vector_int_get(vector2, i2);
		printf("%d was only found in vector2\n", val2);
	}
}

static int test_int_vector_compare(void)
{
	int data;

	struct vector_int vector1;
	vector_int_init(&vector1, 8, &alloc_std);

	data = 0;
	vector_int_push(&vector1, &data);
	data = 2;
	vector_int_push(&vector1, &data);
	data = 4;
	vector_int_push(&vector1, &data);
	data = 6;
	vector_int_push(&vector1, &data);

	struct vector_int vector2;
	vector_int_init(&vector2, 8, &alloc_std);

	data = 1;
	vector_int_push(&vector2, &data);
	data = 2;
	vector_int_push(&vector2, &data);
	data = 4;
	vector_int_push(&vector2, &data);
	data = 7;
	vector_int_push(&vector2, &data);
	data = 9;
	vector_int_push(&vector2, &data);
	data = 11;
	vector_int_push(&vector2, &data);

	vector_int_compare(&vector1, &vector2);

	vector_int_free(&vector1);
	vector_int_free(&vector2);
	return 0;
}

struct test {
	int a;
	char b;
};

VECTOR_DEFINE(vector_test, struct test);

static int test_struct(void)
{
	struct vector_test test_vector;
	vector_test_init(&test_vector, 4, &alloc_std);
	assert(test_vector.len == 0);

	struct test pushed;

	pushed.a = 10;
	pushed.b = 'a';
	vector_test_push(&test_vector, &pushed);
	assert(test_vector.len == 1);

	pushed.a = 20;
	pushed.b = 'b';
	vector_test_push(&test_vector, &pushed);
	assert(test_vector.len == 2);

	pushed.a = 30;
	pushed.b = 'c';
	vector_test_push(&test_vector, &pushed);
	assert(test_vector.len == 3);

	pushed.a = 40;
	pushed.b = 'd';
	vector_test_push(&test_vector, &pushed);
	assert(test_vector.len == 4);

	struct test popped;

	popped.a = -1;
	popped.b = '\0';
	vector_test_pop(&test_vector, &popped);
	assert(popped.a == 40);
	assert(popped.b == 'd');
	assert(test_vector.len == 3);

	popped.a = -1;
	popped.b = '\0';
	vector_test_pop(&test_vector, &popped);
	assert(popped.a == 30);
	assert(popped.b == 'c');
	assert(test_vector.len == 2);

	popped.a = -1;
	popped.b = '\0';
	vector_test_pop(&test_vector, &popped);
	assert(popped.a == 20);
	assert(popped.b == 'b');
	assert(test_vector.len == 1);

	popped.a = -1;
	popped.b = '\0';
	vector_test_pop(&test_vector, &popped);
	assert(popped.a == 10);
	assert(popped.b == 'a');
	assert(test_vector.len == 0);

	vector_test_free(&test_vector);
	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 1) {
		printf("%s takes no arguments.\n", argv[0]);
		return 99;
	}

	if (test_int()) {
		return 1;
	}
	if (test_struct()) {
		return 1;
	}
	if (test_int_vector_compare()) {
		return 1;
	}

	return 0;
}
