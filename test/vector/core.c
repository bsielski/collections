#define _POSIX_C_SOURCE 200809L
#include <collections/vector.h>

#include <assert.h>
#include <stdio.h>

struct test {
	int a;
	char b;
};

static int test_struct(void)
{
	struct _vector test_vector;
	_vector_init(&test_vector, 4, sizeof(struct test), &alloc_std);
	assert(test_vector.len == 0);

	struct test added;

	added.a = 10;
	added.b = 'a';
	_vector_push(&test_vector, &added, sizeof(added));
	assert(test_vector.len == 1);

	added.a = 30;
	added.b = 'c';
	_vector_push(&test_vector, &added, sizeof(added));
	assert(test_vector.len == 2);

	added.a = 20;
	added.b = 'b';
	_vector_insert(&test_vector, 1, &added, 1, sizeof(added));
	assert(test_vector.len == 3);

	added.a = 40;
	added.b = 'd';
	_vector_insert(&test_vector, 3, &added, 1, sizeof(added));
	assert(test_vector.len == 4);

	struct test removed;

	removed.a = -1;
	removed.b = '\0';
	_vector_remove(&test_vector, 1, &removed, 1, sizeof(removed));
	assert(removed.a == 20);
	assert(removed.b == 'b');
	assert(test_vector.len == 3);

	removed.a = -1;
	removed.b = '\0';
	_vector_pop(&test_vector, &removed, sizeof(removed));
	assert(removed.a == 40);
	assert(removed.b == 'd');
	assert(test_vector.len == 2);

	removed.a = -1;
	removed.b = '\0';
	_vector_pop(&test_vector, &removed, sizeof(removed));
	assert(removed.a == 30);
	assert(removed.b == 'c');
	assert(test_vector.len == 1);

	removed.a = -1;
	removed.b = '\0';
	_vector_pop(&test_vector, &removed, sizeof(removed));
	assert(removed.a == 10);
	assert(removed.b == 'a');
	assert(test_vector.len == 0);

	_vector_free(&test_vector);
	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 1) {
		printf("%s takes no arguments.\n", argv[0]);
		return 99;
	}

	if (test_struct()) {
		return 1;
	}

	return 0;
}
