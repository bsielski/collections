#define _POSIX_C_SOURCE 200809L
#include <collections/linked_list.h>

#include <assert.h>
#include <stdio.h>

LINKED_LIST_DEFINE(list_int, int);

static int test_int(void)
{
	struct list_int test_list;
	list_int_init(&test_list, &alloc_std);
	assert(list_int_is_empty(&test_list));

	int pushed = 10;
	list_int_push(&test_list, &pushed);
	assert(!list_int_is_empty(&test_list));

	pushed = 20;
	list_int_push(&test_list, &pushed);
	assert(!list_int_is_empty(&test_list));

	int popped = -1;
	list_int_pop(&test_list, &popped);
	assert(popped == 20);
	assert(!list_int_is_empty(&test_list));

	list_int_pop(&test_list, &popped);
	assert(popped == 10);
	assert(list_int_is_empty(&test_list));

	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 1) {
		printf("%s takes no arguments.\n", argv[0]);
		return 99;
	}

	if (test_int()) {
		return 1;
	}

	return 0;
}
